//Changement 6  dans index.html
//Constantes
//Changement 7 : const btn next ticket
const btnNextTicket = document.getElementById('btnNextTicket');
const btnSubmitTicket = document.getElementById('btnSubmitTicket');
const ticketText = document.getElementById('ticketText') as HTMLInputElement
const selectUser = document.getElementById('selectUserInput')
//Changement 1 : Nouvelle constante
const ticketnumber = document.getElementById('ticketNumber') as HTMLDivElement

//Variables
//Changement 2 : Modifier de constante à variables
let ticketsList = document.getElementById('ticketsList') as HTMLDivElement

//Listeners
btnSubmitTicket.addEventListener('click', postTicket);
//Changement 5 : Ajout Listener
// btnNextTicket.addEventListener('click', patchTickets);

//Fonctions

function getUsers() {
    const url = "https://web-help-request-api.herokuapp.com/users"
    fetch(url)
        .then(response => response.json())
        .then(res => {
            let userId = ''
            res.data.forEach((element:any) => {

                userId += `<option>${element.username}</option>`
            
            });
            document.getElementById('selectUserInput').innerHTML= userId
            console.log(userId);
            
            console.log(res);

            // return res;
            
        })
        
        .catch(err => {
            console.log('Error message: ', err);

        });
}
getUsers();


function postUser() {
    const url = "https://web-help-request-api.herokuapp.com/users"

    fetch(url, {
        method: 'POST',
        body: 'username=Développeur Mobile&password=password',
        headers: {'content-type': 'application/x-www-form-urlencoded'},
    })
        .then(response => response.json())
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log('Error message: ', err);

        });
}


function getTickets() {
    const url = "https://web-help-request-api.herokuapp.com/tickets"
    fetch(url)
        .then(response => response.json())
        .then(res => {
            console.log(res);
            let tickets = '<ul>'
            res.data.forEach((element: any) => {
                tickets += `<li>${element.subject}</li>`

            });
            tickets += '</ul>'
            console.log(tickets)
            ticketsList.innerHTML = tickets; 

            // ticketsList.innerHTML = 'ticketsList'
            // return console.log(res);
        })
        .catch(err => {
            console.log('Error message: ', err);

        });
}
getTickets();

function postTicket() {
    const url = "https://web-help-request-api.herokuapp.com/tickets"

    const text = ticketText.value

    fetch(url, {
        method: 'POST',
        body: `subject=${text}&userId=1`,
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
    })
        .then(response => response.json())
        .then(res => {
            console.log(res);

            return res;
        })
        .catch(err => {
            console.log('Error message: ', err);

        });
}

// Modification 3 : Ajout de la fonction Patch

// function patchTicket() {
//     let url = `https://web-help-request-api.herokuapp.com/tickets/${ticketnumber}`
//     fetch(url)
//     .then(response => response.json())
//         .then(res => {
//             console.log(res);
//             res.data.forEach((element:any) => {
//                 ticketsList -= `<li>${element}</li>`
//             })
//         })
// }

// Modification 4 : Ajout deuxieme fonction Patch

// function patchTickets() {
//     const url = "https://web-help-request-api.herokuapp.com/tickets"
//     fetch(url)
//         .then(response => response.json())
//         .then(res => {
//             console.log(res);
//             let tickets = '<ul>'
//             res.data.forEach((element: any) => {
//                 tickets -= `<li>${element.subject}</li>`

//             });
//             tickets -= '</ul>'
//             console.log(tickets)
//             ticketsList.innerHTML = tickets; 

//             // ticketsList.innerHTML = 'ticketsList'
//             // return console.log(res);
//         })
//         .catch(err => {
//             console.log('Error message: ', err);

//         });
// }
// patchTickets();